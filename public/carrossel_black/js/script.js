$(window).on("load", function () {

    // quando a animação acabar
    var animationEnd = (function (el) {
        var animations = {
            animation: 'animationend',
            OAnimation: 'oAnimationEnd',
            MozAnimation: 'mozAnimationEnd',
            WebkitAnimation: 'webkitAnimationEnd',
        };

        for (var t in animations) {
            if (el.style[t] !== undefined) {
                return animations[t];
            }
        }
    })(document.createElement('div'));

    /////////////


    $('.icones').on('click', function () {
        $(this).addClass('stop');
        refresh();
    });

    $('.img_Box').addClass('animated');

    function refresh() {
        $('.img_Box').removeClass('bounceInRight bounceInLeft bounceOutLeft bounceOutRight');
    }


    $('.right').on('click', function () {
        console.log('direita');
        $(this).closest('.active').addClass('bounceOutLeft').one(animationEnd, function () {
            $(this).addClass('hide');
            $(this).next().addClass('bounceInRight active').removeClass('hide');
        });
    });

    $('.left').on('click', function () {
        console.log('esquerda');
        $(this).closest('.active').addClass('bounceOutRight').one(animationEnd, function () {
            $(this).addClass('hide');
            $(this).prev().addClass('bounceInLeft active').removeClass('hide');
        });
    });


});
