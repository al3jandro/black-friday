import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { ContagemComponent } from "./shared/block/contagem/contagem.component";
import { FooterComponent } from "./shared/block/footer/footer.component";
import { BlogComponent } from "./shared/block/blog/blog.component";
import { ConteudoComponent } from "./shared/block/conteudo/conteudo.component";
import { HomeComponent } from "./layout/home/home.component";
import { PostComponent } from "./shared/block/post/post.component";
import { CountDownModule } from "ng6-countdown";
import { ScrollTopComponent } from "./shared/block/scroll-top/scroll-top.component";
import { ScrollToModule } from "@nicky-lenaers/ngx-scroll-to";
import { OfertasComponent } from "./shared/block/ofertas/ofertas.component";
import { ProductComponent } from "./shared/component/product/product.component";
import { CarouselComponent } from "./shared/block/carousel/carousel.component";
import localePt from "@angular/common/locales/pt";
import { registerLocaleData } from "@angular/common";
import { NgxPaginationModule } from "ngx-pagination";
import { LojaComponent } from './layout/loja/loja.component';
registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    ContagemComponent,
    FooterComponent,
    BlogComponent,
    ConteudoComponent,
    HomeComponent,
    PostComponent,
    ScrollTopComponent,
    OfertasComponent,
    ProductComponent,
    CarouselComponent,
    LojaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CountDownModule,
    AppRoutingModule,
    NgxPaginationModule,
    ScrollToModule.forRoot(),
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
