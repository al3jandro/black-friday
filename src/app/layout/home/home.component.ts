import { ApiService } from "./../../shared/service/api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  clube: any = [];
  down: any = [];
  lojas: any = [];
  radar: any = [];
  black: any = [];
  blog: any = [];
  posts: any = [];
  foot: any = [];
  imagem: any;
  cont: any = [];
  setting: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getJson("db").subscribe(data => {
      this.clube = data.setting[0].clube;
      this.down = data.setting[1].download;
      this.lojas = data.setting[2].lojas;
      this.radar = data.setting[3].radar;
      this.black = data.setting[4].black;
      this.blog = data.setting[5].blogger;
      this.foot = data.setting[6].footer;
      this.cont = data.setting[7].contagem;
      this.getGlogger(this.blog.category);
      this.getSetting();
    });
  }
  getGlogger(cat: number) {
    this.api.getBlog(cat).subscribe(data => {
      this.posts = data;
    });
  }

  getSetting() {
    this.api.getSetting("black-friday").subscribe(data => {
      this.setting = data;
    });
  }
}
