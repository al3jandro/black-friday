import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy
} from "@angular/core";
import { ApiService } from "../../service/api.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-ofertas",
  templateUrl: "./ofertas.component.html",
  styleUrls: ["./ofertas.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OfertasComponent implements OnInit {
  json: any = [];
  shops: any = [];
  lojas: any = [];
  depart: any = [];
  setor: any = [];
  nameLoja: string;
  p = 1;
  constructor(
    private api: ApiService,
    private router: Router,
    private activate: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) {
    this.activate.url.subscribe(u => {
      this.getProducts(223468, +u[0].path);
      this.api.getJson("db").subscribe(data => {
        this.json = data.setting[8].ofertas;
        this.getLojas();
        this.nameLoja = this.onLoja(+u[0].path);
        this.getDepartament();
      });
    });
  }

  ngOnInit() {}

  getProducts(id: number, loja: number) {
    this.api.getFindProductXCampanha(id).subscribe(data => {
      const doug = data["results"];
      doug.forEach(rows => {
        const region = rows.data.lst_preco_regiao;
        region.forEach(el => {
          console.log(loja);
          el.lst_mix_regiao.forEach(element => {
            if (element === loja) {
              this.shops.push({
                emabalagem: rows.data.embalagem_venda.dsc_unidade_venda,
                dsc_produto: rows.dsc_produto,
                dsc_descricao: rows.dsc_descricao,
                dsc_kit: rows.dsc_kit,
                cod_loja: loja,
                cod_produto: rows.cod_produto,
                cod_campanha: rows.cod_campanha,
                cod_regiao: el.cod_regiao,
                departamento: rows.departamento,
                dsc_departamento: rows.dsc_departamento,
                setor: rows.setor,
                dsc_setor: rows.dsc_setor,
                campanhaId: rows.campanhaId,
                vlr_preco_regular: el.vlr_preco_regular,
                vlr_parcela_regular: el.vlr_parcela_regular,
                vlr_preco_clube: el.vlr_preco_clube,
                vlr_parcela_clube: el.vlr_parcela_clube,
                qtd_preco_clube: el.qtd_preco_clube,
                qtd_parcela_clube: el.qtd_parcela_clube
              });
              console.log(this.shops);
              this.cd.detectChanges();
            }
          });
        });
      });
    });
  }
  getLojas() {
    this.api.getLoja().subscribe(data => {
      this.lojas = data;
    });
  }

  onLojas(event) {
    const filter = this.lojas.filter(rows => {
      return rows.cod_loja == event.target.value;
    });
    this.router.navigate([filter[0].cod_loja, filter[0].slug]);
  }

  getDepartament() {
    this.api.getProductParams("menuDepartamento").subscribe(data => {
      this.depart = data.sort((a, b) => {
        return a.nome - b.nome;
      });
    });
  }

  getSector(codigo: number) {
    this.api.getProductParams("menuSetor").subscribe(datas => {
      const filter = datas.filter(rows => {
        return rows.dep_id === codigo;
      });
      this.setor = filter;
    });
  }

  onDepart(event) {
    console.log(event.codigo);
    const filter = this.shops.filter(rows => {
      return rows.departamento == event.codigo;
    });
    this.shops = filter;
  }

  onLoja(id: any) {
    const filter = this.lojas.filter(rows => {
      return rows.cod_loja === id;
    });
    return filter[0].nome;
  }
}
