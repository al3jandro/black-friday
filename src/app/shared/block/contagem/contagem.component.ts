import { ApiService } from "./../../service/api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-contagem",
  templateUrl: "./contagem.component.html",
  styleUrls: ["./contagem.component.scss"]
})
export class ContagemComponent implements OnInit {
  cont: any = [];
  finishDate = "November 29, 2019 00:00";
  isCustomTemplate = true;
  days: any;
  hours: any;
  minutes: any;
  seconds: any;
  backgroundColor = "red";
  textColor = "black";

  constructor(private api: ApiService) {}

  onDaysChanged(days) {
    this.days = days;
  }

  onHoursChanged(hours) {
    this.hours = hours;
  }

  onMinutesChanged(minutes) {
    this.minutes = minutes;
  }

  onSecondsChanged(seconds) {
    this.seconds = seconds;
  }
  ngOnInit() {
    this.api.getJson("db").subscribe(data => {
      this.cont = data.setting[7].contagem;
    });
  }
}
