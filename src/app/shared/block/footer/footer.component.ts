import { ApiService } from "./../../service/api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit {
  foot: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getJson("db").subscribe(data => {
      this.foot = data.setting[6].footer;
    });
  }
}
