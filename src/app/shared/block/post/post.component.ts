import { ApiService } from "./../../service/api.service";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"]
})
export class PostComponent implements OnInit {
  @Input() blog: number;
  item: any = [];
  imagem: any;
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getBlogId(this.blog).subscribe(data => {
      this.item = data;
      this.api.getImage(this.item.featured_media).subscribe(img => {
        // tslint:disable-next-line:no-string-literal
        this.imagem = img["media_details"]["sizes"]["medium"]["source_url"];
      });
    });
  }
}
