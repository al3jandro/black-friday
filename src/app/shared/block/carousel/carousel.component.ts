import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../service/api.service";

@Component({
  selector: "app-carousel",
  templateUrl: "./carousel.component.html",
  styleUrls: ["./carousel.component.scss"]
})
export class CarouselComponent implements OnInit {
  json: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getJson("db").subscribe(data => {
      this.json = data.setting[9].carousel;
    });
  }
}
