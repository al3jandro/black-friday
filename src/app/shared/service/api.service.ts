import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { tap } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class ApiService {
  url = "https://marketing.condor.com.br/api";
  token = "4jUvl8vCW6Dzo0zkouII9wgJ4Mf9Y5YWzToTaaElCPOWqOkO8duUa0kbT6VkgoG0";
  constructor(private http: HttpClient) {}

  getJson(json: string): Observable<any> {
    return this.http.get(`./assets/json/${json}.json`);
  }

  getBlog(category: number): Observable<any[]> {
    if (category === 0) {
      return this.http.get<any[]>(
        "https://www.condor.com.br/blog/wp-json/wp/v2/posts",
        {
          params: {
            per_page: "3"
          }
        }
      );
    } else {
      return this.http.get<any[]>(
        "https://www.condor.com.br/blog/wp-json/wp/v2/posts",
        {
          params: {
            per_page: "3",
            categories: `${category}`
          }
        }
      );
    }
  }

  getBlogId(id: number): Observable<any[]> {
    return this.http.get<any[]>(
      `https://www.condor.com.br/blog/wp-json/wp/v2/posts/${id}`
    );
  }

  getImage(id: number): Observable<any[]> {
    return this.http.get<any[]>(
      `https://www.condor.com.br/blog/wp-json/wp/v2/media/${id}`
    );
  }

  getSetting(slug: string): Observable<any[]> {
    return this.http
      .get<any[]>(
        `${this.url}/Cooperadas/?filter[where][slug]=${slug}&filter[order]=ordem&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }

  /**
   * Produtos
   */
  getFindProductXCampanha(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(
        `${this.url}/Produtos/findCampanha?code=${id}&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }

  getFindId(table: string, id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${this.url}/${table}/${id}?access_token=${this.token}`)
      .pipe(tap(data => data));
  }

  getLoja(): Observable<any[]> {
    return this.http
      .get<any[]>(`${this.url}/Produtos/loja?access_token=${this.token}`)
      .pipe(tap(data => data));
  }

  getProductParams(params: string, collection: string = ""): Observable<any[]> {
    return this.http
      .get<any[]>(
        `${this.url}/Produtos/${params}?&access_token=${this.token}&${collection}`
      )
      .pipe(tap(data => data));
  }
}
