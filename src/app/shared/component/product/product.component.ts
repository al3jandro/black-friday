import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { ApiService } from "../../service/api.service";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {
  @Input() code: any;
  start: string;
  end: string;
  public shops: any = [];
  imagem: string;
  valor: number;
  items: any = [];
  constructor(private api: ApiService, private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.getVigencia(this.code.campanhaId);
    this.imagem = `https://marketing.condor.com.br/api/Containers/produtos/download/${this.code.cod_produto}.jpg`;
  }

  getVigencia(campanha: number) {
    this.api.getFindId("Campanhas", campanha).subscribe(row => {
      this.start = row["vigencia_inicio"];
      this.end = row["vigencia_fin"];
    });
  }
}
